﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cut_Image {


	public class Walker { 
		public int posX, posY;
		public int dirX, dirY;
		public byte[,] markers;
		public Bitmap image;
		public Color borderColor = Color.Black;

		public Walker(int posX, int posY, byte[,] markers, Bitmap image, int dirX, int dirY, Color borderColor) { 
			this.posX = posX;
			this.posY = posY;
			this.dirX = dirX;
			this.dirY = dirY;
			this.markers = markers;
			this.image = image;
			this.borderColor = borderColor;
		}
	}


	public class AreaWalker : Walker {
		
		public int initX, initY, initDirX, initDirY;
		public int top, bottom, right, left;

		public AreaWalker(int posX, int posY, byte[,] markers, Bitmap image, int dirX, int dirY, Color borderColor) : base(posX, posY, markers, image, dirX, dirY, borderColor) {  
			initX = posX;
			initY = posY;
			initDirX = dirX;
			initDirY = dirY;
			top = initY;
			bottom = initY;
			right = initX;
			left = initX;
		}

		public void expand() { 
			left = Math.Min(left, posX);
			right = Math.Max(right, posX);

			top = Math.Min(top, posY);
			bottom = Math.Max(bottom, posY);
		}

		private static int walkerCounter = 0;

		public void walk() {
			bool firstStep = true;
			int firstStepCounter = 0;
			do {
				if(posX == initX && posY == initY) { 
					firstStepCounter++;
					if(firstStepCounter == 8) { 
						return;
					}
				}

				int nextPosX = posX + dirX;
				int nextPosY = posY + dirY;
				markers[posX, posY] = Consts.MARKER_AREA;
				expand();
				

				int rightDirX = dirX;
				int rightDirY = dirY;
				Utils.rotateCW(ref rightDirX, ref rightDirY);

				int rightPosX = posX + rightDirX;
				int rightPosY = posY + rightDirY;
				if (Utils.hasPixel(rightPosX, rightPosY, image) && !Utils.checkSomeColor(image.GetPixel(rightPosX, rightPosY), borderColor, Consts.PIXELS_COMPARE_STEP)) {

					Utils.rotateCW(ref dirX, ref dirY);
					nextPosX = posX + dirX;
					nextPosY = posY + dirY;
					posX = nextPosX;
					posY = nextPosY;
					continue;
				} 


				if(!Utils.hasPixel(nextPosX, nextPosY, image) || Utils.checkSomeColor(image.GetPixel(nextPosX, nextPosY), borderColor, Consts.PIXELS_COMPARE_STEP)) {
					Utils.rotateCCW(ref dirX, ref dirY);
					continue;
				} else { 
					posX = nextPosX;
					posY = nextPosY;
				}
				
				if(firstStep) { 
					if(posX == initX && posY == initY) { 
						initDirX = dirX;
						initDirY = dirY;
					} else {
						firstStep = false;
					}
				}

				
			
			} while(posX != initX || posY != initY || dirX != initDirX || dirY != initDirY || firstStep);
			
			Program.MainWindowInstance.addCutArea(Box.fromTBLR(top, bottom, left, right));
		}
	}

	public class BackgroundWalker : Walker {
		
		bool wasBorder = false;
		public BackgroundWalker(int posX, int posY, byte[,] markers, Bitmap image, Color borderColor) : base(posX, posY, markers, image, 0, 0, borderColor) {}

		public void walk() {
			for(posY = 0; posY < image.Height; posY++) { 
				dirX = 1; dirY = 0;
				wasBorder = true;
				for(posX = 0; posX < image.Width; posX++) { 
					if(!check()) {
						
						if(wasBorder && Utils.getMarkerSafe(posX, posY, markers) != Consts.MARKER_AREA) {
								int currentDirX = dirX;
								int currentDirY = dirY;
								Utils.rotateCW(ref currentDirX, ref currentDirY);
								AreaWalker walker = new AreaWalker(posX, posY, markers, image, currentDirX, currentDirY, borderColor);
								walker.walk();
						}
						wasBorder = false;
					} else {
						wasBorder = true;
					}
				}

				dirX = -1; dirY = 0;
				wasBorder = true;
				for(posX = image.Width - 1; posX >= 0; posX--) {
					
					if(!check()) {
						if(wasBorder && Utils.getMarkerSafe(posX, posY, markers) != Consts.MARKER_AREA) {
								int currentDirX = dirX;
								int currentDirY = dirY;
								Utils.rotateCW(ref currentDirX, ref currentDirY);
								AreaWalker walker = new AreaWalker(posX, posY, markers, image, currentDirX, currentDirY, borderColor);
								walker.walk();
						}
						wasBorder = false;
					} else {
						wasBorder = true;
					}
				}
			}
		}
		
		private bool isDrawMarker(byte marker) { 
			return marker == Consts.MARKER_BORDER || marker == Consts.MARKER_NOPIXELS;
		}

		public bool check() { 
			Color currentColor = image.GetPixel(posX, posY);
			if(!Utils.checkSomeColor(currentColor, borderColor, Consts.PIXELS_COMPARE_STEP))
				return false;

			if(isDrawMarker(Utils.getMarkerSafe(posX - 1, posY, markers)) || 
				isDrawMarker(Utils.getMarkerSafe(posX + 1, posY, markers)) ||
				isDrawMarker(Utils.getMarkerSafe(posX, posY - 1, markers))) {
				markers[posX, posY] = Consts.MARKER_BORDER;
				return true;
			}
			return false;
		}
	}
}
