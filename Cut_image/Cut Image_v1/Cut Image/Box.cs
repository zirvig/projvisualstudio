﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cut_Image {
	public class Box { 
		public float X { get ; set; }
		public float Y { get; set; }
		public float Width { get; set; }
		public float Height { get; set; }
		
		public float AspectRatio { get { return Width / Height; } }

		public float Top { 
			get { return Y + Height; } 
			set { Height = value - Bottom; }
		}
		public float Bottom { 
			get { 
				return Y; 
			} 
			set { 
				Height = Top - value; 
				Y = value; 
			}
		}
		public float Left { get { return X; } set { Width = Right - value; X = value;} }
		public float Right { get { return X + Width; } set { Width = value - Left;} }

		private Box(float x, float y, float width, float height) { 
			this.X = x;
			this.Y = y;
			this.Width = width;
			this.Height = height;
		}


		public static Box fromXYWH(float x, float y, float width, float height) { 
			return new Box(x, y, width, height);
		}


		public static Box fromTBLR(float top, float bottom, float left, float right) { 
			return new Box(left, bottom, right - left, top - bottom);
		}

		public Box convertTo(float fromX1, float fromX2, float toX1, float toX2, float fromY1, float fromY2, float toY1, float toY2) { 
			return fromTBLR(
				Utils.convertCoord(fromY1, fromY2, toY1, toY2, Top), 
				Utils.convertCoord(fromY1, fromY2, toY1, toY2, Bottom),
				Utils.convertCoord(fromX1, fromX2, toX1, toX2, Left), 
				Utils.convertCoord(fromX1, fromX2, toX1, toX2, Right)
			);
		}

		public Box convertTo(Box fromCoords, Box toCoords) {
			return convertTo(fromCoords.Left, fromCoords.Right, toCoords.Left, toCoords.Right, fromCoords.Top, fromCoords.Bottom, toCoords.Top, toCoords.Bottom);
		}


		public override string ToString() {
			return "(" + X + "; " + Y + ")" + "; w: " + Width + " h: " + Height;
		}

		public string ToString2() {
			return Top + "; " + Bottom  + "; " + Left + "; " + Right;
		}

	}
}
