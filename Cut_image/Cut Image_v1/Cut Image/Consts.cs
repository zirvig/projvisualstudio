﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cut_Image {
	public static class Consts {
		public const byte MARKER_CLEAR = 0;
		public const byte MARKER_BORDER = 1;
		public const byte MARKER_AREA = 2;

		public const byte MARKER_NOPIXELS = 255;

		public const int PIXELS_COMPARE_STEP = 10; 
	}
}
