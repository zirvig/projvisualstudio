﻿namespace Cut_Image {
	partial class MainWindow {
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent() {
            this.OpenImageButton = new System.Windows.Forms.Button();
            this.cutMethodCombobox = new System.Windows.Forms.ComboBox();
            this.commonPropertiesPanel = new System.Windows.Forms.Panel();
            this.tableCutModePanel = new System.Windows.Forms.Panel();
            this.defaultCutOkButton = new System.Windows.Forms.Button();
            this.borderOffsetTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.rowsTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.colsTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.picturePanel = new System.Windows.Forms.Panel();
            this.saveButton = new System.Windows.Forms.Button();
            this.tableCutModePanel.SuspendLayout();
            this.picturePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // OpenImageButton
            // 
            this.OpenImageButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenImageButton.Location = new System.Drawing.Point(747, 29);
            this.OpenImageButton.Name = "OpenImageButton";
            this.OpenImageButton.Size = new System.Drawing.Size(184, 23);
            this.OpenImageButton.TabIndex = 1;
            this.OpenImageButton.Text = "Открыть изображение";
            this.OpenImageButton.UseVisualStyleBackColor = true;
            this.OpenImageButton.Click += new System.EventHandler(this.OpenImageButton_Click);
            // 
            // cutMethodCombobox
            // 
            this.cutMethodCombobox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cutMethodCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cutMethodCombobox.FormattingEnabled = true;
            this.cutMethodCombobox.Location = new System.Drawing.Point(747, 149);
            this.cutMethodCombobox.Name = "cutMethodCombobox";
            this.cutMethodCombobox.Size = new System.Drawing.Size(184, 21);
            this.cutMethodCombobox.TabIndex = 2;
            this.cutMethodCombobox.SelectedIndexChanged += new System.EventHandler(this.cutMethodCombobox_SelectedIndexChanged);
            // 
            // commonPropertiesPanel
            // 
            this.commonPropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.commonPropertiesPanel.Location = new System.Drawing.Point(639, 210);
            this.commonPropertiesPanel.Name = "commonPropertiesPanel";
            this.commonPropertiesPanel.Size = new System.Drawing.Size(292, 355);
            this.commonPropertiesPanel.TabIndex = 3;
            // 
            // tableCutModePanel
            // 
            this.tableCutModePanel.Controls.Add(this.defaultCutOkButton);
            this.tableCutModePanel.Controls.Add(this.borderOffsetTextBox);
            this.tableCutModePanel.Controls.Add(this.label4);
            this.tableCutModePanel.Controls.Add(this.rowsTextBox);
            this.tableCutModePanel.Controls.Add(this.label3);
            this.tableCutModePanel.Controls.Add(this.label1);
            this.tableCutModePanel.Controls.Add(this.colsTextBox);
            this.tableCutModePanel.Location = new System.Drawing.Point(3, 392);
            this.tableCutModePanel.Name = "tableCutModePanel";
            this.tableCutModePanel.Size = new System.Drawing.Size(272, 119);
            this.tableCutModePanel.TabIndex = 4;
            // 
            // defaultCutOkButton
            // 
            this.defaultCutOkButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.defaultCutOkButton.Location = new System.Drawing.Point(80, 93);
            this.defaultCutOkButton.Name = "defaultCutOkButton";
            this.defaultCutOkButton.Size = new System.Drawing.Size(118, 23);
            this.defaultCutOkButton.TabIndex = 6;
            this.defaultCutOkButton.Text = "ОК";
            this.defaultCutOkButton.UseVisualStyleBackColor = true;
            this.defaultCutOkButton.Click += new System.EventHandler(this.recalcButton_Click);
            // 
            // borderOffsetTextBox
            // 
            this.borderOffsetTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.borderOffsetTextBox.Location = new System.Drawing.Point(125, 55);
            this.borderOffsetTextBox.Name = "borderOffsetTextBox";
            this.borderOffsetTextBox.Size = new System.Drawing.Size(132, 20);
            this.borderOffsetTextBox.TabIndex = 5;
            this.borderOffsetTextBox.Text = "0";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(77, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Отступ";
            // 
            // rowsTextBox
            // 
            this.rowsTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rowsTextBox.Location = new System.Drawing.Point(125, 29);
            this.rowsTextBox.Name = "rowsTextBox";
            this.rowsTextBox.Size = new System.Drawing.Size(132, 20);
            this.rowsTextBox.TabIndex = 3;
            this.rowsTextBox.Text = "3";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Количество строк";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Количество столбцов";
            // 
            // colsTextBox
            // 
            this.colsTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.colsTextBox.Location = new System.Drawing.Point(125, 3);
            this.colsTextBox.Name = "colsTextBox";
            this.colsTextBox.Size = new System.Drawing.Size(132, 20);
            this.colsTextBox.TabIndex = 0;
            this.colsTextBox.Text = "3";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(699, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Режим";
            // 
            // picturePanel
            // 
            this.picturePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picturePanel.BackColor = System.Drawing.Color.DimGray;
            this.picturePanel.Controls.Add(this.tableCutModePanel);
            this.picturePanel.Location = new System.Drawing.Point(12, 12);
            this.picturePanel.Name = "picturePanel";
            this.picturePanel.Size = new System.Drawing.Size(621, 553);
            this.picturePanel.TabIndex = 7;
            this.picturePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.picturePanel_Paint);
            this.picturePanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
            this.picturePanel.MouseLeave += new System.EventHandler(this.picturePanel_MouseLeave);
            this.picturePanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
            this.picturePanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseUp);
            this.picturePanel.Resize += new System.EventHandler(this.picturePanel_Resize);
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveButton.Location = new System.Drawing.Point(747, 58);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(184, 23);
            this.saveButton.TabIndex = 8;
            this.saveButton.Text = "Сохранить";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 577);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.commonPropertiesPanel);
            this.Controls.Add(this.cutMethodCombobox);
            this.Controls.Add(this.OpenImageButton);
            this.Controls.Add(this.picturePanel);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.tableCutModePanel.ResumeLayout(false);
            this.tableCutModePanel.PerformLayout();
            this.picturePanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Button OpenImageButton;
		private System.Windows.Forms.ComboBox cutMethodCombobox;
		private System.Windows.Forms.Panel commonPropertiesPanel;
		private System.Windows.Forms.Panel tableCutModePanel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox colsTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox borderOffsetTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox rowsTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button defaultCutOkButton;
		private System.Windows.Forms.Panel picturePanel;
		private System.Windows.Forms.Button saveButton;
	}
}

