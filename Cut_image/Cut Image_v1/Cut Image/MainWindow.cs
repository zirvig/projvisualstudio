﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cut_Image {
	public partial class MainWindow : Form {

		Color CutAreasColor = Color.FromArgb(0, 225, 255);

		public MainWindow() {
			InitializeComponent();
		}


		List<CutMethodItem> cutMethods = new List<CutMethodItem>();

		private void MainWindow_Load(object sender, EventArgs e) {

			cutMethods.Add(new CutMethodItem("Обычный режим", tableCut, tableCutModePanel));
			cutMethods.Add(new CutMethodItem("По линии-разделителю", linesCut));
			cutMethods.Add(new CutMethodItem("По фону", bgCut));
			cutMethods.Add(new CutMethodItem("Заданные области", null, null, true));


			foreach(CutMethodItem cutMethodItem in cutMethods) { 
				if(cutMethodItem.PropertiesPanel == null)
					continue;

				commonPropertiesPanel.Controls.Add(cutMethodItem.PropertiesPanel);
				cutMethodItem.PropertiesPanel.Visible = false;
				cutMethodItem.PropertiesPanel.Dock = DockStyle.Fill;
			}
			cutMethodCombobox.DataSource = cutMethods; //Устанавливаем объекты методов вырезания в качестве источника данных выпадающего списка


			onResize();

			recalcButton_Click(null, null);
		}

		Bitmap handleBitmap;
		public List<Box> cutAreas = new List<Box>();
		byte[,] markers;

		Box imageCoordsBox;
		float imageAspectRatio;

		public void addCutArea(Box area) {
			if(area.Width > 2 && Math.Abs(area.Height) > 2) { 
				cutAreas.Add(area);
			}
		}

		private void OpenImageButton_Click(object sender, EventArgs e) {
			OpenFileDialog openImageDialog = new OpenFileDialog();
			if(openImageDialog.ShowDialog() == DialogResult.OK) { 
				loadImage(openImageDialog.FileName);
			}
		}

		public void loadImage(string path) {
			handleBitmap = new Bitmap(path);
			imageAspectRatio = ((float)handleBitmap.Width) / ((float)handleBitmap.Height);
			float imageY = -0.5f;
			float imageHeight = 1.0f;
			float imageX = -imageAspectRatio / 2.0f;
			float imageWidth = imageAspectRatio;
			imageCoordsBox = Box.fromXYWH(imageX, imageY, imageWidth, imageHeight);
			
			markers = new byte[handleBitmap.Width, handleBitmap.Height];
			recalcButton_Click(null, null);
			redraw();
		}

		private void showMethodPropertiesPanel(CutMethodItem cutMethodItem) { 
			foreach(CutMethodItem currentCutMethodItem in cutMethods) { 
				if(currentCutMethodItem.PropertiesPanel == null)
					continue;

				currentCutMethodItem.PropertiesPanel.Visible = currentCutMethodItem == cutMethodItem;
			}
		}

		private void cutMethodCombobox_SelectedIndexChanged(object sender, EventArgs e) {
			CutMethodItem currentMethodItem = (CutMethodItem)cutMethodCombobox.SelectedItem;
			showMethodPropertiesPanel(currentMethodItem);
			recalcButton_Click(null, null);
		}

		public void clearSelection() { 
			cutAreas.Clear();
		}


		private void recalcButton_Click(object sender, EventArgs e) {
			if(handleBitmap == null)
				return;

			clearSelection();
			markers = new byte[handleBitmap.Width, handleBitmap.Height];
			CutMethodItem cutMethodItem = (CutMethodItem)cutMethodCombobox.SelectedItem;
			
			if(cutMethodItem.Method != null)
				cutMethodItem.Method();

			userDraw = cutMethodItem.UserDraw;

			redraw(true);
		}

		private void picturePanel_Paint(object sender, PaintEventArgs e) {
			if(!isRenderInited)
				return;

			Graphics graphics = picturePanel.CreateGraphics();
			graphics.DrawImage(imageBitmap, 0, 0, picturePanel.Width, picturePanel.Height);
			graphics.DrawImage(interfaceBitmap, 0, 0, picturePanel.Width, picturePanel.Height);
			graphics.Dispose();
		}


		float viewportWidth;
		float viewportHeight;
		float viewportAspectRatio;

		Box viewportCoordsBox;
		Box viewportScreenBox;

		private float toVpx(float x) {
			return Utils.convertCoord(-viewportAspectRatio / 2.0f, viewportAspectRatio / 2.0f, 0, viewportWidth, x);
		}

		private float toVpy(float y) {
			return Utils.convertCoord(-0.5f, 0.5f, viewportHeight, 0, y);
		}

		public float toVpDistance(float distance) {
			return Utils.convertDistance(-0.5f, 0.5f, 0, viewportHeight, distance);
		}

		public float toCoordsDistance(float vpDistance) {
			return Utils.convertDistance(0, viewportHeight, -0.5f, 0.5f, vpDistance);
		}

		private float toXCoord(float x) {
			return Utils.convertCoord(0, viewportWidth, -viewportAspectRatio / 2.0f, viewportAspectRatio / 2.0f, x);
		}

		private float toYCoord(float y) { 
			return Utils.convertCoord(viewportHeight, 0, -0.5f, 0.5f, y);
		}

		private Box toCoordsBox(Box pixelBox) { 
			return pixelBox.convertTo(viewportScreenBox, viewportCoordsBox);
		}

		private Box toVpBox(Box coordsBox) { 
			return coordsBox.convertTo(viewportCoordsBox, viewportScreenBox);
		}

		Bitmap imageBitmap;
		Bitmap interfaceBitmap;

		private void redraw(bool interfaceOnly = false) {
			if(!isRenderInited)
				return;

			if(!interfaceOnly) {
				redrawImage();
			}
			redrawInterface();
			picturePanel.Invalidate();
		}

		private void redrawImage() {
			if(handleBitmap == null)
				return;

			Graphics graphics = Graphics.FromImage(imageBitmap);
			graphics.Clear(Color.Transparent);
			Box vpBox = toVpBox(imageCoordsBox);
			graphics.DrawImage(handleBitmap, vpBox.Left, vpBox.Top, vpBox.Width, Math.Abs(vpBox.Height));
			graphics.Dispose();

		}

		private void redrawInterface() {
			Graphics graphics = Graphics.FromImage(interfaceBitmap);
			graphics.Clear(Color.Transparent);
			Pen pen = new Pen(CutAreasColor, 3);

			foreach(Box cutArea in cutAreas) {
				Box coordsBox = cutArea.convertTo(0, handleBitmap.Width, imageCoordsBox.Left, imageCoordsBox.Right, 0, handleBitmap.Height, imageCoordsBox.Top, imageCoordsBox.Bottom);
				Box vpBox = toVpBox(coordsBox);
				graphics.DrawRectangle(pen, vpBox.Left, vpBox.Top, vpBox.Width, Math.Abs(vpBox.Height));
				
			}
			graphics.Dispose();
			picturePanel.Invalidate();
		}

		bool isRenderInited = false; 

		private void onResize() {
			viewportWidth	= picturePanel.Width;
			viewportHeight	=  picturePanel.Height;


			if(viewportWidth == 0 || viewportHeight == 0)
				return;
			viewportAspectRatio = viewportWidth / viewportHeight;
			isRenderInited = true;
			
			viewportCoordsBox = Box.fromTBLR(0.5f, -0.5f, -viewportAspectRatio / 2.0f, viewportAspectRatio / 2.0f);
			if(imageBitmap != null)
				imageBitmap.Dispose();
			if(interfaceBitmap != null)
				interfaceBitmap.Dispose();
			viewportScreenBox = Box.fromTBLR(0.0f, viewportHeight, 0.0f, viewportWidth);
			imageBitmap = new Bitmap(picturePanel.Width, picturePanel.Height);
			interfaceBitmap = new Bitmap(picturePanel.Width, picturePanel.Height);

			redraw();
		}

		private void picturePanel_Resize(object sender, EventArgs e) {
			onResize();
		}

		private void tableCut() {
			int colsCount = int.Parse(colsTextBox.Text);
			int rowsCount = int.Parse(rowsTextBox.Text);
			int cellBorder = int.Parse(borderOffsetTextBox.Text);

			float cellHeight = ((float)handleBitmap.Height) / (float)rowsCount;
			float cellWidth = ((float)handleBitmap.Width) / (float)colsCount;
			 
			for (int i = 0; i < rowsCount; i++) { 
				for(int j = 0; j < colsCount; j++) { 
					cutAreas.Add(Box.fromTBLR(cellHeight * i + cellBorder, cellHeight * (i + 1.0f) - cellBorder, cellWidth * j + cellBorder, cellWidth * (j + 1.0f) - cellBorder));
				}
			}
		}

		class ColorIntPair { 
			public Color color;
			public int count;
		}

		public void bgCut() {

			List<ColorIntPair> colorsCounter = new List<ColorIntPair>();
			for(int x = 0; x < handleBitmap.Width; x++) { 
				Color currentColor = handleBitmap.GetPixel(x, 0);
				ColorIntPair p = colorsCounter.Find((pair) => Utils.checkSomeColor(pair.color, currentColor, Consts.PIXELS_COMPARE_STEP));	
				if(p == null)
					colorsCounter.Add(new ColorIntPair { color = currentColor, count =  1 });
				else
					p.count++;
			}

			var pairsArray = colorsCounter.ToArray();
			Array.Sort(pairsArray, (pair1, pair2) => pair1.count - pair2.count);
			Color borderColor = pairsArray[pairsArray.Length - 1].color;
				


			markers = new byte[handleBitmap.Width, handleBitmap.Height];
			BackgroundWalker walker = new BackgroundWalker(0, 0, markers, handleBitmap, borderColor);
			walker.walk();

			redraw();
		}


		public void linesCut() {
			List<ColorIntPair> colorsCounter = new List<ColorIntPair>();
			for(int x = 0; x < handleBitmap.Width; x++) { 
				Color currentColor = handleBitmap.GetPixel(x, 0);
				ColorIntPair p = colorsCounter.Find((pair) => Utils.checkSomeColor(pair.color, currentColor, Consts.PIXELS_COMPARE_STEP));	
				if(p == null)
					colorsCounter.Add(new ColorIntPair { color = currentColor, count =  1 });
				else
					p.count++;
			}

			var pairsArray = colorsCounter.ToArray();
			Array.Sort(pairsArray, (pair1, pair2) => pair1.count - pair2.count);
			Color borderColor = pairsArray[0].color;
				


			markers = new byte[handleBitmap.Width, handleBitmap.Height];
			BackgroundWalker walker = new BackgroundWalker(0, 0, markers, handleBitmap, borderColor);
			walker.walk();

			redraw();
		}


		public bool userDraw = false;
		public bool isMousePressed = false;
		Box currentDrawBox;

		public void onDrawStart(float cursorX, float cursorY) { 
			isMousePressed = true;
			float coordX = toXCoord(cursorX);
			float coordY = toYCoord(cursorY);
			
			float imageX = Utils.convertCoord(imageCoordsBox.Left, imageCoordsBox.Right, 0, handleBitmap.Width, coordX);
			float imageY = Utils.convertCoord(imageCoordsBox.Top, imageCoordsBox.Bottom, 0, handleBitmap.Height, coordY);
			currentDrawBox = Box.fromTBLR(imageY, imageY, imageX, imageX);
			cutAreas.Add(currentDrawBox);
			redraw(true);
		}

		public void onDrawStop() { 
			if(isMousePressed)
				isMousePressed = false;

			if(currentDrawBox != null) {
				currentDrawBox = null;
			}
		}

		private void pictureBox_MouseUp(object sender, MouseEventArgs e) {
			isMousePressed = false;
			onDrawStop();
		}

		private void pictureBox_MouseMove(object sender, MouseEventArgs e) {
			if(isMousePressed && currentDrawBox != null) {
				float coordX = toXCoord(e.Location.X);
				float coordY = toYCoord(e.Location.Y);
			
				float imageX = Utils.convertCoord(imageCoordsBox.Left, imageCoordsBox.Right, 0, handleBitmap.Width, coordX);
				float imageY = Utils.convertCoord(imageCoordsBox.Top, imageCoordsBox.Bottom, 0, handleBitmap.Height, coordY);
				currentDrawBox.Right = imageX;
				currentDrawBox.Bottom = imageY;
				redraw(true);
			}
		}

		private void pictureBox_MouseDown(object sender, MouseEventArgs e) {
			if(!userDraw)
				return;

			onDrawStart(e.Location.X, e.Location.Y);
		}

		private void picturePanel_MouseLeave(object sender, EventArgs e) {
			onDrawStop();
		}


		private void saveButton_Click(object sender, EventArgs e) {
			var fbd = new FolderBrowserDialog();
			DialogResult result = fbd.ShowDialog();
			if (result != DialogResult.OK || string.IsNullOrWhiteSpace(fbd.SelectedPath)) {
				return;
			}

			int counter = 1;
			foreach(Box cutArea in cutAreas) { 
				var bitmap = new Bitmap((int)cutArea.Width, (int)Math.Abs(cutArea.Height));
				using (var g = Graphics.FromImage(bitmap))
				{
					g.DrawImage(handleBitmap, 0, 0, new Rectangle((int)cutArea.Left, (int)cutArea.Top, (int)cutArea.Width, (int)Math.Abs(cutArea.Height)), GraphicsUnit.Pixel);
				}
				bitmap.Save(fbd.SelectedPath + "/region_" + counter + ".png");

				bitmap.Dispose();
				counter++;
			}
		}
	}


	public class CutMethodItem { 
		public Action Method { get; set; }
		public Panel PropertiesPanel { get; set; }
		public string Description { get; set; }

		public bool UserDraw { get; set; }

		public CutMethodItem(string description, Action method, Panel propertiesPanel = null, bool userDraw = false) {
			this.Method = method;
			this.PropertiesPanel = propertiesPanel;
			this.Description = description;
			this.UserDraw = userDraw;
		}

		public override string ToString() {
			return Description;
		}
	}

}
