﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cut_Image {
	public static class Utils {


		public static float convertCoord(float from1, float from2, float to1, float to2, float coord) { 
			float dif = (to2 - to1) / (from2 - from1);
			float fromDist = coord - from1;
			return to1 + fromDist * dif;
		}

		public static float convertDistance(float from1, float from2, float to1, float to2, float distance) {
			float dif = (to2 - to1) / (from2 - from1);
			return distance * dif;
		}

		public static void rotateCCW(ref int x, ref int y) { 
			int resultX = y;
			int resultY = -x;
			x = resultX;
			y = resultY;
		}

		public static void rotateCW(ref int x, ref int y) { 
			int resultX = -y;
			int resultY = x;
			x = resultX;
			y = resultY;
		}


		public static bool hasPixel(int x, int y, Bitmap image) {
			return (0 <= x && x < image.Width) && (0 <= y && y < image.Height);
		}

		public static byte getMarkerSafe(int x, int y, byte[,] markers) { 
			if((0 <= x && x < markers.GetLength(0)) && (0 <= y && y < markers.GetLength(1)))
				return markers[x, y];
			else
				return Consts.MARKER_NOPIXELS; 
		}


		public static bool checkSomeColor(Color color1, Color color2, int step = 0) { 
			return (Math.Abs(color2.R - color1.R) <= step) && (Math.Abs(color2.G - color1.G) <= step) && (Math.Abs(color2.B - color1.B) <= step);
		}
	}
}
