﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace FinalApplication
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            
        }

        private void startGame(object sender, EventArgs e)
        {
            Form1 game = new Form1();
            game.ShowDialog();
        }

        private void getDescription(object sender, EventArgs e)
        {
            Description des = new Description();
            des.ShowDialog();
        }

        private void exit(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
