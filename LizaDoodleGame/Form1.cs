﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace LizaDoodleGame
{
    public partial class Form1 : Form
    {
        SoundPlayer gg = new SoundPlayer(@"Z:\ZiRViG\Desktop\phon.wav");
        SoundPlayer ll = new SoundPlayer(@"Z:\ZiRViG\Desktop\over.wav");
        //SoundPlayer cc = new SoundPlayer(@"Z:\ZiRViG\Desktop\coin.wav");
        public Form1()
        {
            gg.Play();
            InitializeComponent();
        }

        int score;

        void game_over ()
        {
            if(player.Bounds.IntersectsWith(ground.Bounds))
            {
                gg.Stop();
                ll.Play();
                timer1.Stop();
                MessageBox.Show("Ты отчислен :)");
            }
        }
        void Game_Logic()
        {
            foreach(Control x in this.Controls)
            {
                if(x is PictureBox && x.Tag=="base")
                {
                    if (player.Bounds.IntersectsWith(x.Bounds))
                    {
                        player.Top = x.Top - player.Height;
                        if(player.Top > 200)
                        {
                            player.Top -= 100;
                        }
                    }
                    if (player.Bounds.IntersectsWith(x.Bounds)==player.Top>200)
                    {
                        x.Top += 10;
                        if(x.Top>500)
                        {
                            //cc.Play();
                            score += 1;
                            lbl_score.Text = "Пересдач : " + score;

                            x.Top = 0;
                        }
                    }
                }
            }
        }
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.KeyCode)
            {
                case Keys.Right:
                    player.Image = Properties.Resources.lizaright;
                    if (player.Right < 300)
                        player.Left += 10;
                    if (player.Top > 200)
                        player.Top -= 10;
                    break;
                case Keys.Left:
                    player.Image = Properties.Resources.lizaleft;
                    if (player.Left > 0)
                        player.Left -= 10;
                    if (player.Top > 200)
                        player.Top -= 10;
                    break;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Game_Logic();
            player.Top += 10;
            game_over();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
