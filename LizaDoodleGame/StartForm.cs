﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace LizaDoodleGame
{
    public partial class StartForm : Form
    {
        SoundPlayer sf = new SoundPlayer(@"Z:\ZiRViG\Desktop\button.wav");
        public StartForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sf.Play();
            Form1 newForm = new Form1();
            newForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            sf.Play();
            string a = "Смирнова Елизавета 181-352";
            string b = "Игра сделана по рофлу";
            string с = "Я прыгаю по пересдачам";
            MessageBox.Show(a + "\n" + b + "\n" + с, "About Me");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            sf.Play();
            MessageBox.Show("Кукусики", "Уже уходишь ?");
            this.Close();
        }

        private void StartForm_Load(object sender, EventArgs e)
        {

        }
    }
}
