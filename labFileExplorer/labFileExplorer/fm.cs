﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFileExplorer
{
    public partial class fm : Form
    {
        private string _curDir;

        public fm()
        {
            InitializeComponent();
            CurDir = Directory.GetCurrentDirectory();
            buUp.Click += (s, e) => LoadDir(Directory.GetParent(CurDir).ToString());
            edDir.KeyDown += EdDir_KeyDown;

            toolStripMenuItem2.Click += (s, e) => lv.View = View.LargeIcon;
            toolStripMenuItem3.Click += (s, e) => lv.View = View.SmallIcon;
            toolStripMenuItem4.Click += (s, e) => lv.View = View.List;
            toolStripMenuItem5.Click += (s, e) => lv.View = View.Details;
            toolStripMenuItem6.Click += (s, e) => lv.View = View.Tile;

            buDirSellect.Click += BuDirSellect_Click;

            lv.ItemSelectionChanged += (s, e) => SelItem = Path.Combine(CurDir, e.Item.Text);
            lv.DoubleClick += (s, e) => LoadDir(SelItem);
            ////1
            //var c1 = new ColumnHeader();
            //c1.Text = "Name";
            //c1.Width = 350;
            //lv.Columns.Add(c1);
            ////2
            //lv.Columns.Add(new ColumnHeader() { Text = "Name", Width = 350 });
            ////3
            lv.Columns.Add("Имя", 350);
            lv.Columns.Add("Дата Изменения", 150);
            lv.Columns.Add("Тип", 100);
            lv.Columns.Add("Размер", 150);

            //Text += ": Drivers" + string.Join("", Directory.GetLogicalDrives());
            LoadDir(CurDir);
        }

        private void BuDirSellect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                LoadDir(dialog.SelectedPath);
            }
        }

        private void EdDir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadDir(edDir.Text);
            }
        }

        private void LoadDir(string newDir)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(newDir);
            lv.BeginUpdate();
            lv.Items.Clear();
            foreach (var item in directoryInfo.GetDirectories())
            {
                var f = new FileInfo(item.FullName);
                lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Папка", f.Length.ToString() + "байт" }, 0));
            }
            foreach (var item in directoryInfo.GetFiles())
            {
                var f = new FileInfo(item.FullName);
                lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", f.Length.ToString() + "байт" }, 1));
            }
            lv.EndUpdate();
            CurDir = newDir;
        }

        public string CurDir
        {
            get
            {
                return _curDir;
            }
            private set
            {
                _curDir = value;
                edDir.Text = value;
            }
        }
        public string SelItem { get; private set; }
    }
}
